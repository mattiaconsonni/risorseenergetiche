import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RisorsaEnergeticaTest {

    private RisorsaEnergetica risorsaEnergetica;

    @BeforeEach
    void setUp() {
        risorsaEnergetica = new RisorsaEnergetica(NomeRisorsa.PETROLIO, 6, 50, 100000);
    }

    @Test
    void testCostruttore() {
        assertNotNull(risorsaEnergetica);
    }

    @Test
    void testNome() {
        assertEquals(NomeRisorsa.PETROLIO, risorsaEnergetica.getNome());
    }

    @Test
    void testPrezzo() {
        assertEquals(100000, risorsaEnergetica.getPrezzo());
    }

    @Test
    void testCostoTotale() {
        assertEquals(200000, risorsaEnergetica.calcolaCosto(2));
    }

}