import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CentraleEnergeticaTest {

    private CentraleEnergetica centraleEnergetica;
    private RisorsaEnergetica risorsaEnergetica1;
    private RisorsaEnergetica risorsaEnergetica2;

    @BeforeEach
    void setUp() {
        centraleEnergetica = new CentraleEnergetica(0.8, 3);
        risorsaEnergetica1 = new RisorsaEnergetica(NomeRisorsa.PETROLIO, 6, 50, 100000);
        risorsaEnergetica2 = new RisorsaEnergetica(NomeRisorsa.GAS_NATURALE, 1, 70, 50000);
    }

    @Test
    void testCostruttore() {
        assertNotNull(centraleEnergetica);
    }

    @Test
    void testAggiuntaRisorse() {
        assertEquals(0,centraleEnergetica.getNumRisorse());
        centraleEnergetica.aggiungiRisorsa(risorsaEnergetica1);
        assertEquals(1, centraleEnergetica.getNumRisorse());
        centraleEnergetica.aggiungiRisorsa(risorsaEnergetica1);
        centraleEnergetica.aggiungiRisorsa(risorsaEnergetica2);
        assertEquals(3, centraleEnergetica.getNumRisorse());
    }

    @Test
    void testProduzioneEnergetica() {
        centraleEnergetica.aggiungiRisorsa(risorsaEnergetica1);
        assertEquals(50 * 0.8, centraleEnergetica.calcolaProduzioneEnergetica());
        centraleEnergetica.aggiungiRisorsa(risorsaEnergetica1);
        assertEquals(2 * 50 * 0.8, centraleEnergetica.calcolaProduzioneEnergetica());
        centraleEnergetica.aggiungiRisorsa(risorsaEnergetica2);
        assertEquals(2 * 50 * 0.8 + 70 * 0.8, centraleEnergetica.calcolaProduzioneEnergetica());
    }

    @Test
    void testCalcolaCostoEnergia() {
        centraleEnergetica.aggiungiRisorsa(risorsaEnergetica1);
        assertEquals(100000, centraleEnergetica.calcoloCostoEnergia(60));
        centraleEnergetica.aggiungiRisorsa(risorsaEnergetica1);
        assertEquals(200000, centraleEnergetica.calcoloCostoEnergia(100));
        centraleEnergetica.aggiungiRisorsa(risorsaEnergetica2);
        assertEquals(250000, centraleEnergetica.calcoloCostoEnergia(200));
    }
}