/**
 * Classe che modella il funzionamento di una centrale energetica.
 */
public class CentraleEnergetica {
    private RisorsaEnergetica[] risorse;
    private int numRisorse;
    private double efficienza;

    /**
     * Costruttore che si occupa di creare un'istanza di centrale energetica.
     * @param efficienza Double che rappresenta l'efficienza in trasformazione dell'energia della centrale.
     * @param maxRisorse Intero che rappresenta il numero massimo di risorse utilizzabili.
     */
    public CentraleEnergetica(double efficienza, int maxRisorse) {
        this.risorse = new RisorsaEnergetica[maxRisorse];
        this.efficienza = efficienza;
    }

    /**
     * Metodo che aggiunga una risorsa alla centrale se c'è ancora spazio o se essa è disponibile.
     * @param risorsaEnergetica La risorsa energetica da aggiungere.
     */
    public void aggiungiRisorsa(RisorsaEnergetica risorsaEnergetica) {
        if (this.numRisorse != this.risorse.length && risorsaEnergetica.getQuantitaDisponibile() > 0) {
            this.risorse[numRisorse++] = risorsaEnergetica;
            System.out.println("Risorsa aggiunta!");
        }
        else if (risorsaEnergetica.getQuantitaDisponibile() == 0)
            System.out.println("Esaurita la risorsa.");
        else
            System.out.println("Hai raggiunto il numero massimo di risorse.");
    }

    /**
     * Metodo che visualizza tutte le risorse della centrale.
     */
    public void visualizzaRisorse() {
        System.out.println("Lista delle risorse: ");
        for (int i = 0; i < this.numRisorse; i++) {
            System.out.println("Risorsa n°" + i + ": " + this.risorse[i].getNome());
        }
    }

    /**
     * Metodo che trasforma la risorsa in energie tenendo conto dell'efficienza della centrale.
     * @param risorsaEnergetica La risorsa energetica da trasformare.
     * @return Un double che rappresente l'energia derivata dalla trasformazione della risorsa.
     */
    public double trasformaRisorsa(RisorsaEnergetica risorsaEnergetica) {
        return risorsaEnergetica.getPotenzaCalorifica() * this.efficienza;
    }

    /**
     * Metodo che calcola la produzione energetica ottenuta da tutte le risorse.
     * @return Double che rappresenta la produzione energetica dell'azienda.
     */
    public double calcolaProduzioneEnergetica() {
        double somma = 0;
        for (int i = 0; i < this.numRisorse; i++) {
            somma += this.trasformaRisorsa(this.risorse[i]);
        }
        return somma;
    }
    
    public void simulaConsumo(double consumo) {
        double produzione = this.calcolaProduzioneEnergetica();
        if (consumo > produzione)
            System.out.println("Non c'è abbastanza produzione energetica per questa richiesta!");
        else {
            for (int i = this.numRisorse - 1; i >= 0; i--) {
                if ((consumo - this.trasformaRisorsa(this.risorse[i]) < 0)) {
                    this.risorse = null;
                    this.numRisorse--;
                }
                else {
                    consumo -= this.trasformaRisorsa(this.risorse[i]);
                    this.risorse = null;
                    this.numRisorse--;
                }
            }
            System.out.println("Lavoro completato: rimangono esattamente " + (produzione - consumo) + " energia.");
        }
    }

    public double calcoloCostoEnergia(double consumo) {
        double costo = 0;
        double produzione = 0;
        for (int i = 0; i < this.numRisorse; i++) {
            if (produzione > consumo)
                break;
            else {
                costo += this.risorse[i].getPrezzo();
                produzione += this.trasformaRisorsa(this.risorse[i]);
            }
        }
        return costo;
    }

    public RisorsaEnergetica[] getRisorse() {
        return risorse;
    }

    public int getNumRisorse() {
        return numRisorse;
    }
}
