/**
 * Classe che modella una risorsa energetica.
 */
public class RisorsaEnergetica {
    private NomeRisorsa nomeRisorsa;
    private double quantitaDisponibile;
    private double potenzaCalorifica;
    private double prezzo;

    /**
     * Costruttore che crea una Risorsa energetica con il suo nome, la quantità disponibile di essa, la sua potenza calorifica e il relativo prezzo.
     * @param nomeRisorsa Nome della risorsa energetica.
     * @param quantitaDisponibile Double che rappresenta la quantità disponibile di risorsa.
     * @param potenzaCalorifica Double che rappresenta la potenza calorifica della risorsa.
     * @param prezzo Double che rappresenta il prezzo della risorsa.
     */
    public RisorsaEnergetica(NomeRisorsa nomeRisorsa, double quantitaDisponibile, double potenzaCalorifica, double prezzo) {
        this.nomeRisorsa = nomeRisorsa;
        this.quantitaDisponibile = quantitaDisponibile;
        this.potenzaCalorifica = potenzaCalorifica;
        this.prezzo = prezzo;
    }

    /**
     * Metodo che calcola il costo di una quantità della risorsa.
     * @param quantita Intero che rappresenta la quantità di risorsa.
     * @return
     */
    public double calcolaCosto(double quantita) {
        return this.prezzo * quantita;
    }

    public NomeRisorsa getNome() {
        return this.nomeRisorsa;
    }

    public double getQuantitaDisponibile() {
        return this.quantitaDisponibile;
    }

    public void setQuantitaDisponibile(double quantitaDisponibile) {
        this.quantitaDisponibile = quantitaDisponibile;
    }

    public double getPotenzaCalorifica() {
        return potenzaCalorifica;
    }

    public double getPrezzo() {
        return prezzo;
    }

}
